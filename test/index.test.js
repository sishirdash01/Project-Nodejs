var chai = require('chai');
var chaiHttp = require('chai-http');
var expect = chai.expect;

chai.use(chaiHttp);

describe('Server', function() {
  it('should respond to /', function(done) {
    chai.request('http://localhost:3700')
      .get('/')
      .end(function(err, res) {
        expect(res).to.have.status(200);
        done();
      });
  });
});